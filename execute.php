<?php
session_start();

//data dari ajax
$message = $_POST['message'];

if ($message == '' || $message == null) {
    return;
}

if ($_SESSION['message'] == null) {
    //kalau session kosong

    $dataSession = array();
    array_push($dataSession, $message);
    $_SESSION["message"] = $dataSession;
} else {
    //kalau session ada isinya dan akan ditimpa

    $dataSession = $_SESSION["message"];
    array_push($dataSession, $message);
    $_SESSION["message"] = $dataSession;
}
?>