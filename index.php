<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP MOD 5</title>

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>

    <!-- untuk css -->
    <link rel="stylesheet" href="style.css">

    <!-- untuk script js -->
    <script src="script.js"></script>
</head>

<body>
    <h1>TP MODUL 5</h1>
    <input type="text" id="text-input" />
    <button onclick="tambah()">Tambah</button>

    <div class="data">
        <?php
        if (!empty($_SESSION['message'])) {
            for ($i=0; $i < count($_SESSION['message']); $i++) { 
                $data = $_SESSION['message'][$i];

                echo $data . " = ";
                echo md5($data);

                echo "<br>";
            }
        }
        ?>
    </div>
</body>

</html>