//fungsi untuk menambah data di session
function tambah() {
    
    //get data dari input text
    var message = document.getElementById("text-input").value;

    //execute via ajax
    $.ajax({
        url: 'execute.php',
        type: 'POST',
        data: 'message=' + message,
        success: function (data) {
            location.reload();
        }
    });
}